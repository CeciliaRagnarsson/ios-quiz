//
//  GameLogic.m
//  Quiz
//
//  Created by Cragnarsson on 2016-02-04.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import "GameLogic.h"
#import "Question.h"

@implementation GameLogic

-(instancetype)init {
    self = [super init];
    if(self){
        self.questions = [[NSArray alloc]init];
        self.usedQuestions = [[NSMutableArray alloc] init];
        self.currentQuestionIndex = 1;
        self.correctGuesses = 0;
        self.wrongGuesses = 0;
    }
    return self;
}


- (void) createArray{
    
    Question *questionOne = [[Question alloc] init];
    Question *questionTwo = [[Question alloc] init];
    Question *questionThree = [[Question alloc] init];
    Question *questionFour = [[Question alloc] init];
    Question *questionFive = [[Question alloc] init];
    Question *questionSix = [[Question alloc] init];
    Question *questionSeven = [[Question alloc] init];
    Question *questionEight = [[Question alloc] init];
    Question *questionNine = [[Question alloc] init];
    Question *questionTen = [[Question alloc] init];
    
    questionOne.questionText = @"När vann ABBA Eurovision med låten 'Waterloo'?";
    questionOne.rightAnswer = @"1974";
    questionOne.answerOne = @"1971";
    questionOne.answerTwo = @"1972";
    questionOne.answerThree = @"1973";
    questionOne.answerFour = @"1974";

    questionTwo.questionText= @"Vilket land har vunnit Eurovision Song Contest flest gånger?";
    questionTwo.rightAnswer = @"Irland";
    questionTwo.answerOne = @"Sverige";
    questionTwo.answerTwo = @"Azerbadjan";
    questionTwo.answerThree = @"Irland";
    questionTwo.answerFour = @"Frankrike";
    
    questionThree.questionText = @"Vilket land har kommit sist i Eurovision Song Contest flest gånger?";
    questionThree.rightAnswer = @"Norge";
    questionThree.answerOne = @"Norge";
    questionThree.answerTwo = @"England";
    questionThree.answerThree = @"Holland";
    questionThree.answerFour = @"Rumänien";
    
    questionFour.questionText = @"Vilket land har deltagit i Eurovision Song Contest flest gånger?";
    questionFour.rightAnswer = @"Tyskland";
    questionFour.answerOne = @"Storbritannien";
    questionFour.answerTwo = @"Tyskland";
    questionFour.answerThree = @"Frankrike";
    questionFour.answerFour = @"Belgien";
    
    questionFive.questionText = @"Hur långt får ett bidrag vara som längst i Eurovision Song Contest?";
    questionFive.rightAnswer = @"3 min";
    questionFive.answerOne = @"2 min";
    questionFive.answerTwo = @"2,5 min";
    questionFive.answerThree = @"3 min";
    questionFive.answerFour = @"3,5 min";
    
    questionSix.questionText = @"Hur gammal måste man vara för att delta i Eurovision Song Contest?";
    questionSix.rightAnswer = @"16";
    questionSix.answerOne = @"15";
    questionSix.answerTwo = @"16";
    questionSix.answerThree = @"17";
    questionSix.answerFour = @"18";
    
    questionSeven.questionText = @"Hur många gånger har Sverige vunnit Eurovision Song Contest?";
    questionSeven.rightAnswer = @"6 gånger";
    questionSeven.answerOne = @"4 gånger";
    questionSeven.answerTwo = @"5 gånger";
    questionSeven.answerThree = @"6 gånger";
    questionSeven.answerFour = @"7 gånger";
    
    questionEight.questionText = @"Vilken land var värdland första gången Eurovision Song Contest gick av stapeln?";
    questionEight.rightAnswer = @"Schweiz";
    questionEight.answerOne = @"Schweiz";
    questionEight.answerTwo = @"Frankrike";
    questionEight.answerThree = @"Storbritannien";
    questionEight.answerFour = @"Irland";
    
    questionNine.questionText = @"Vilket år startade Eurovision Song Contest?";
    questionNine.rightAnswer = @"1956";
    questionNine.answerOne = @"1952";
    questionNine.answerTwo = @"1954";
    questionNine.answerThree = @"1956";
    questionNine.answerFour = @"1958";
    
    questionTen.questionText = @"Med vilken låt vann Carola Eurovision Song Contest år 1991?";
    questionTen.rightAnswer = @"Fångad av en stormvind";
    questionTen.answerOne = @"Främling";
    questionTen.answerTwo = @"Fångad av en stormvind";
    questionTen.answerThree = @"Mitt i ett äventyr";
    questionTen.answerFour = @"Mickey";
    
    
    self.questions = @[questionOne,
                       questionTwo,
                       questionThree,
                       questionFour,
                       questionFive,
                       questionSix,
                       questionSeven,
                       questionEight,
                       questionNine,
                       questionTen];
    
}

- (Question *) generateNewQuestion {
    
    Question *randomQuestion = self.questions[arc4random() % [self.questions count]];
    
        if ([self.usedQuestions containsObject:randomQuestion]){
            NSLog(@" fanns i listan");
            return [self generateNewQuestion];
        }else{
            [self.usedQuestions addObject:randomQuestion];
              NSLog(@" else");
    }
    NSLog(@"return");
    self.currentQuestionIndex ++;
    return randomQuestion;
}

- (BOOL *) checkIfGuess:(NSString *)guess
      equalsRightAnswer:(NSString *)rightAnswer{
    
    if([guess isEqualToString:rightAnswer]){
        self.correctGuesses++;
        return YES;
        
    }else{
        self.wrongGuesses++;
        return NO;
    }
    
}



@end
