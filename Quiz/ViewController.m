//
//  ViewController.m
//  Quiz
//
//  Created by Cragnarsson on 2016-02-04.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import "ViewController.h"
#import "GameLogic.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIButton *answer1;
@property (weak, nonatomic) IBOutlet UIButton *answer2;
@property (weak, nonatomic) IBOutlet UIButton *answer3;
@property (weak, nonatomic) IBOutlet UIButton *answer4;
@property (weak, nonatomic) IBOutlet UITextView *result;
@property (weak, nonatomic) IBOutlet UIButton *next;
@property (nonatomic) GameLogic *gameLogic;
@property (nonatomic) Question *question;
@property (weak, nonatomic) IBOutlet UITextView *questiontext;
@property (weak, nonatomic) IBOutlet UILabel *questionNumberLabel;
@property (nonatomic) NSString *questionNumber;
@property (weak, nonatomic) IBOutlet UIButton *playAgainButton;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.gameLogic = [[GameLogic alloc] init];
    [self.gameLogic createArray];
    [self getNewQuestion];
}

- (void) getNewQuestion {
    int currentQuestion = self.gameLogic.currentQuestionIndex;
    self.result.hidden = YES;
    self.next.hidden = YES;
    self.answer1.hidden = NO;
    self.answer2.hidden = NO;
    self.answer3.hidden = NO;
    self.answer4.hidden = NO;
    self.questionNumberLabel.hidden = NO;
    self.playAgainButton.hidden = YES;
    self.question = [self.gameLogic generateNewQuestion];
    self.questiontext.text = self.question.questionText;
    self.questionNumber = [NSString stringWithFormat:@"Fråga %i", currentQuestion];
    self.questionNumberLabel.text = self.questionNumber;
    
    [self.answer1 setTitle: self.question.answerOne forState:UIControlStateNormal];
    [self.answer2 setTitle: self.question.answerTwo forState:UIControlStateNormal];
    [self.answer3 setTitle: self.question.answerThree forState:UIControlStateNormal];
    [self.answer4 setTitle: self.question.answerFour forState:UIControlStateNormal];
    
}
- (IBAction)playAgain:(id)sender {
    [self viewDidLoad];
}

- (IBAction)answerButtonClicked:(id)sender {
    
     NSString *buttonName = [sender titleForState:UIControlStateNormal];
    self.result.hidden = NO;
    self.next.hidden = NO;
    
    if([self.gameLogic checkIfGuess:buttonName equalsRightAnswer:self.question.rightAnswer]){
        self.result.text = @"Grattis, Ditt svar var rätt!";
    }else{
        self.result.text = @"Tyvärr, Ditt svar var fel!";
    }
}

- (IBAction)nextQuestionButton:(id)sender {
    if(self.gameLogic.currentQuestionIndex <= 5){
        [self getNewQuestion];
    }else{
        self.questionNumberLabel.hidden = YES;
        self.result.hidden = YES;
        self.answer1.hidden = YES;
        self.answer2.hidden = YES;
        self.answer3.hidden = YES;
        self.answer4.hidden = YES;
        self.next.hidden = YES;
        self.playAgainButton.hidden = NO;
        
        int correctGuesses = self.gameLogic.correctGuesses;
        int wrongGuesses = self.gameLogic.wrongGuesses;
        self.questiontext.text = [NSString stringWithFormat:@"Spelet är slut. \nDu hade %i rätt och %i fel! ", correctGuesses, wrongGuesses];
    }
    
    
}

@end
