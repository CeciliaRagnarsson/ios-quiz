//
//  GameLogic.h
//  Quiz
//
//  Created by Cragnarsson on 2016-02-04.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Question.h"

@interface GameLogic : NSObject

- (void) createArray;
- (Question *) generateNewQuestion;
- (BOOL *) checkIfGuess:(NSString *)guess
      equalsRightAnswer:(NSString *)rightAnswer;

@property (nonatomic) int currentQuestionIndex;
@property (nonatomic) int correctGuesses;
@property (nonatomic) int wrongGuesses;

@property (strong, nonatomic) NSArray* questions;
@property (strong, nonatomic) NSMutableArray* usedQuestions;




@end
