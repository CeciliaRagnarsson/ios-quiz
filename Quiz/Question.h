//
//  Question.h
//  Quiz
//
//  Created by Cragnarsson on 2016-02-05.
//  Copyright © 2016 Cecilia Ragnarsson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Question : NSObject

@property (nonatomic) NSString* questionText;
@property (nonatomic) NSString* rightAnswer;
@property (nonatomic) NSString* answerOne;
@property (nonatomic) NSString* answerTwo;
@property (nonatomic) NSString* answerThree;
@property (nonatomic) NSString* answerFour;



@end
